import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackendRoutingModule } from './backend-routing.module';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { Router, RouterModule } from '@angular/router';
import { StyleClassModule } from 'primeng/styleclass';
import { RippleModule } from 'primeng/ripple';
import { AppConfigModule } from 'src/app/layout/config/config.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from "primeng/inputtext";
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MessagesModule } from 'primeng/messages';
import { ToastModule } from 'primeng/toast';
import { ProgressBarModule } from 'primeng/progressbar';
import { TreeSelectModule } from 'primeng/treeselect';
import { AutoCompleteModule } from "primeng/autocomplete";
import { BlockUIModule } from 'primeng/blockui';
import { TagModule } from 'primeng/tag';
import { FileUploadModule } from 'primeng/fileupload';
import { InputNumberModule } from 'primeng/inputnumber';
import { TooltipModule } from 'primeng/tooltip';

// component pages
import { HomeComponent} from './components/home/home.component';
import { SlotsComponent } from './components/slots/slots.component';
import { UsersComponent } from './components/users/users.component';
import { RolesComponent } from './components/roles/roles.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClinicsComponent } from './components/clinics/clinics.component';
import { PeriodsComponent } from './components/periods/periods.component';
import { ManagementsComponent } from './components/managements/managements.component';
import { ServicesComponent } from './components/services/services.component';
import { HospitalsComponent } from './components/hospitals/hospitals.component';
import { CalendarModule } from 'primeng/calendar';


 

@NgModule({
    imports: [
        CommonModule,
        BackendRoutingModule,
        ButtonModule,
        RouterModule,
        StyleClassModule,
        RippleModule,
        AppConfigModule,
        DialogModule,
        FormsModule,
        RadioButtonModule,
        InputTextModule,
        InputSwitchModule,
        TableModule,
        CalendarModule,
        DropdownModule,
        AutoCompleteModule,
        ProgressSpinnerModule,
        MessagesModule,
        ToastModule,
        ProgressBarModule,
        TreeSelectModule,
        BlockUIModule,TagModule
        ,AutoCompleteModule,
        BlockUIModule,
        FileUploadModule
        ,AutoCompleteModule,
        InputNumberModule,
        TooltipModule

    ],
    declarations: [
        HomeComponent,
        SlotsComponent,
        UsersComponent,
        RolesComponent,
        DashboardComponent,
        ClinicsComponent,
        PeriodsComponent,
        ManagementsComponent,
        ServicesComponent,
        HospitalsComponent
    ]
})
export class BackendModule {

    constructor(private router: Router) {}
}
