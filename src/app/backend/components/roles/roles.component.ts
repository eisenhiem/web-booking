import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { LayoutService } from '../../../layout/service/app.layout.service';
import { RolesService } from './roles.service';

@Component({
    selector: 'app-backend-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent {
    blockedPanel: boolean = false;

    roles: any;
    displayForm: boolean = false;
    rolesName: string = '';
    rolesActive: boolean = true;
    rolesIdEdit: number = 0;
    rolesNameEdit: string = '';
    rolesActiveEdit: boolean = true;
    isAdd: boolean = false;
    isEdit: boolean = false;

    rolesStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false }
    ];

    constructor(
        private layoutService: LayoutService,
        private rolesService: RolesService
    ) {
    }

    async ngOnInit() {
        await this.getData();
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
    }

    // get Services data from API
    async getData() {
        this.blockedPanel = true;
        try {
            const res: any = await this.rolesService.list();
            let roles = [];
            roles = res.data;

            if (roles.results.length > 0) {
                this.roles = roles.results;
               
            } else {
                alert('Error loading roles');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;
            console.log(error);
        }
    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.rolesName = '';
    }

    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.rolesIdEdit = data.role_id;
        this.rolesNameEdit = data.role_name;
        this.rolesActiveEdit = data.is_active;
        this.displayForm = true;
    }

    // function save data
    async save() {
        let data = {
            role_name: this.rolesName
        };

        // save data
        await this.rolesService.save(data);

        // close form
        this.displayForm = false;

        //refresh data
        await this.getData();
    }

    // function update data
    async update() {
        let id = this.rolesIdEdit;
        let body = {
            role_name: this.rolesNameEdit,
            is_active: this.rolesActiveEdit
        };

        // update data
        await this.rolesService.update(id, body);

        // close form
        this.displayForm = false;

        // refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false
        };
        await this.rolesService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true
        };
        await this.rolesService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}