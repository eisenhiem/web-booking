import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LayoutService } from '../../../layout/service/app.layout.service';
import { HomeService } from './home.service';
import { LookupService } from '../../services/lookup.service';
import { ManagementsService } from '../managements/managements.service'; 
import { Table } from 'primeng/table';
import * as _ from 'lodash';

@Component({
    selector: 'app-backend-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    // breadcrumb items
    blockedPanel: boolean = false;
    home: any;
    homeData: any = [];
    loading: boolean = true;
    slot_type: any = [];
    bookings: any[] = [];
    listServices: any;
    listPeriods: any;
    currentDate: any;

    total: number = 0;
    confirm: number = 0;
    today: number = 0;
    cardData: any = [];
    listServiceType: any;
    is_admin:boolean = false;

    @ViewChild('filter') filter!: ElementRef;
    constructor(
        private layoutService: LayoutService,
        private homeService: HomeService,
        private lookupService: LookupService,
        private managementsService: ManagementsService,
    ) {
        let roleId = sessionStorage.getItem('userRole');
        if(roleId == '2') {
            this.is_admin = true;
        }
    }

    async ngOnInit() {
        await this.getService();
        await this.getPeriod();
        await this.getData();
        this.loading = false;
    }

    // get Services data from API
    async getData() {
        this.currentDate = await this.thaiDateFormat(new Date().toISOString().substring(0,10));
       
        this.blockedPanel = true;
        let hospitalID: any = sessionStorage.getItem('hospitalId');
        let serviceID: any = sessionStorage.getItem('serviceId') || null;
        this.cardData = [];
        this.total = 0;
        this.confirm = 0;
        this.today = 0;
        this.bookings = [];
        
        try {
            const res: any = await this.homeService.list(hospitalID, serviceID);
            if(serviceID){
                this.listServiceType = this.listServices.filter((x: any) => x.service_id == serviceID);
            } else {
                this.listServiceType = this.listServices;
            }
            for(let x of this.listServiceType){
                this.blockedPanel = true;
                const res_dashboard = await this.homeService.getDashboard(hospitalID, x.service_type_id); 
                // console.log(res_dashboard);
                let data = {
                    "service_type_name":x.service_type_name,
                    "total":res_dashboard.data.results.data_total,
                    "confirm":res_dashboard.data.results.data_confirm,
                    "today":res_dashboard.data.results.data_day
                    }
                this.total += Number(res_dashboard.data.results.data_total);
                this.confirm += Number(res_dashboard.data.results.data_confirm);
                this.today += Number(res_dashboard.data.results.data_day);
                this.cardData.push(data);               
            }

            let bookings_: any = await res.data;

            if (bookings_.results.length > 0) {

                for (let v of bookings_.results) {
                    let for_v = v.reserve;

                        for (let s of for_v) {
                            
                            // console.log("currentDate",this.currentDate);
                            // console.log("thaiDateFormat",this.thaiDateFormat(s.slot_date));
                            // ดึงข้อมูลเฉพาะผู้รับบริการวันนี้
                            if(this.thaiDateFormat(s.slot_date) == this.currentDate && s.status != "cancel"){
                                console.log(s.reserve_id,s.status);
                                let p: any = this.listPeriods.find(
                                    (x: any) => x.period_id === s.period_id
                                );
                                s.period_name = p.period_name;
        
                                let sv: any = this.listServices.find(
                                    (x: any) => x.service_type_id === s.service_type_id
                                );
                                s.service_type_name = sv.service_type_name;
                                
                                if(!s.is_confirm){
                                    s.is_confirm = "รอยืนยัน"
                                }else{
                                    s.is_confirm = "ยืนยันแล้ว"
                                }

                                s.slot_shortdate = this.thaiDateFormat(s.slot_date);

                                this.bookings.push(s);
                            }
                           
                        }
                       
                }

            } else {
                // alert('Error loading DATA');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;
            console.log(error);
        }

    }


    onGlobalFilter(table: Table, event: Event) {

        table.filterGlobal((event.target as HTMLInputElement).value, 'contains');

    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = '';
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

    thaiDateFormat(date: any) {
        const thaiMonth = [
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.',
        ];
        const newDate = new Date(date);
        let day = newDate.getDate();
        let monthName = thaiMonth[newDate.getMonth()];
        let thaiYear = newDate.getFullYear() + 543;

        let thaidate = day + ' ' + monthName + ' ' + thaiYear;
        return thaidate;
    }

    async getService() {
        const res: any = await this.lookupService.listServiceType();
        let service = [];
        service = res.data;
        if (service.results.length > 0) {
            this.listServices = service.results;
        } else {
            alert('error load period');
        }
    }

    async getPeriod() {
        const res: any = await this.lookupService.listPeriod();
        let period = [];
        period = res.data;

        if (period.results.length > 0) {
            this.listPeriods = period.results;
        } else {
            alert('error load period');
        }
    }

    async isClose(reserve_id: any) {
        let body = {
            status: "close",
        };
        await this.managementsService.update(reserve_id, body);

        // refresh data
        await this.getData();
    }

    async isMiss(item: any) {
        let body = {
            status: "miss",
        };
        let id = item.reserve_id;
        
        await this.managementsService.update(id, body);

        // check customer miss 3 times
        let customer_id = item.customer_id;
        let res = await this.homeService.getCustomerById(customer_id);
        if(res.data.results.length > 0){
            let data = res.data.results;
            let i = 0;
            data.find((element: any, index: any) => {
                if(element.status == "miss"){
                    i++;
                }
            });
            if(i >=3 ){
                // set expite date + 30 days
                let expire_date = new Date();
                expire_date.setDate(expire_date.getDate() + 30);
                let info = {
                    expire_ban_date: expire_date.toISOString().substring(0,10),
                };
                await this.homeService.updateCustomer(customer_id, info);
            }      
        }
        
        // refresh data
        await this.getData();
    }
}