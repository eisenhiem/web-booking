import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';

import { LayoutService } from '../../../layout/service/app.layout.service';
import { HospitalsService } from './hospitals.service';

@Component({
    selector: 'app-backend-hospitals',
    templateUrl: './hospitals.component.html',
    styleUrls: ['./hospitals.component.scss'],
})
export class HospitalsComponent {
    blockedPanel: boolean = false;

    hospitals: any;
    displayForm: boolean = false;

    hospitalCode: string = '';
    hospitalName: string = '';
    hospitalsActive: boolean = true;

    hospitalCodeEdit: string = '';
    hospitalIdEdit: number = 0;
    hospitalNameEdit: string = '';
    hospitalActiveEdit: boolean = true;

    isAdd: boolean = false;
    isEdit: boolean = false;

    hospitalsStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    constructor(
        private layoutService: LayoutService,
        private hospitalsService: HospitalsService
    ) {}

    async ngOnInit() {
        await this.getData();
    }
    
    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    // get Services data from API
    async getData() {
        this.blockedPanel = true;
        try {
            const res: any = await this.hospitalsService.list();
            let hospitals = [];
            hospitals = res.data;

            if (hospitals.results.length > 0) {
                this.hospitals = hospitals.results;
            } else {
                alert('Error loading hospitals');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;

            console.log(error);
        }
    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.hospitalCode = '';
        this.hospitalName = '';
    }

    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.hospitalIdEdit = data.hospital_id;
        this.hospitalCodeEdit = data.hospital_code;
        this.hospitalNameEdit = data.hospital_name;
        this.hospitalActiveEdit = data.is_active;
        this.displayForm = true;
    }

    // function save data
    async save() {
        let data = {
            hospital_code: this.hospitalCode,
            hospital_name: this.hospitalName,
        };

        // save data
        await this.hospitalsService.save(data);

        // close form
        this.displayForm = false;

        //refresh data
        await this.getData();
    }

    // function update data
    async update() {
        let id = this.hospitalIdEdit;
        let body = {
            hospital_code: this.hospitalCodeEdit,
            hospital_name: this.hospitalNameEdit,
            is_active: this.hospitalActiveEdit,
        };

        // update data
        await this.hospitalsService.update(id, body);

        // close form
        this.displayForm = false;

        // refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false,
        };
        await this.hospitalsService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true,
        };
        await this.hospitalsService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}
