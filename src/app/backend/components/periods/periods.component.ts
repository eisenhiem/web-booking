import { Component } from '@angular/core';
import { LayoutService } from '../../../layout/service/app.layout.service';
import { PeriodsService } from  './periods.service';
import { Message, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-backend-periods',
    templateUrl: './periods.component.html',
    styleUrls: ['./periods.component.scss'],
    providers: [MessageService]
})
export class PeriodsComponent {
    blockedPanel: boolean = false;
    msgs: Message[] = [];
    messages: any | undefined;
    hospitalId = sessionStorage.getItem('hospitalId');


    periods: any;
    displayForm: boolean = false;

    periodsName :string = '';
    periodsActive :boolean = true;
    hospital_id:any;
    start_time:any;
    end_time:any;

    periodsIdEdit :number = 0;
    periodsNameEdit :string = '';
    periodsActiveEdit :boolean = true;

    isAdd :boolean = false;
    isEdit :boolean = false;
    isSpiner: boolean = false;
    periodsStatus = [
        {label: 'Active', value: true},
        {label: 'Inactive', value: false}
    ];

    constructor(
        private layoutService: LayoutService,
        private periodsService:PeriodsService,
        private messageService: MessageService,
        ) {

        }
    async ngOnInit() {
        await this.getData();
    }
    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
    }
        // get Services data from API
        async getData() {
            this.blockedPanel = true;

            const res:any  = await this.periodsService.getByHospitalID(this.hospitalId);
            let periods = [];
            periods = res.data;
            
            if(periods.results.length > 0){
                this.periods = periods.results;
                this.blockedPanel = false;
            } else {
                alert('Error loading periods');
                this.blockedPanel = false;
            }
        }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.periodsName = '';
        this.hospital_id= this.hospitalId;
        this.start_time='';
        this.end_time='';
    }

    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.periodsIdEdit = data.period_id;
        this.periodsNameEdit = data.period_name;
        this.periodsActiveEdit = data.is_active;
        this.displayForm = true;
        this.hospital_id= data.hospital_id;
        this.start_time= data.start_time;
        this.end_time= data.end_time;
        this.messages = [];
    }

    // function save data
    async save() {
        let data = {
            period_name: this.periodsName,
            hospital_id: this.hospitalId,
            start_time: this.start_time,
            end_time: this.end_time
        };

        try {
            if(this.periodsName !=''){
                this.isSpiner = true;
                const res:any = await this.periodsService.save(data);

                let message_save = [];
                    message_save = res.data;
                    if(message_save.ok){
                        this.showSuccessViaToast('success','Success','บันทึกสำเร็จ');
                        this.messages = [];
                    } else {
                        this.showErrorViaMessages('error','Error','บันทึกสำเร็จไม่สำเร็จ'  );
                    }
                // close form
                this.displayForm = false;

                //refresh data
                await this.getData();
                this.isSpiner = false;

            }else{
                this.showErrorViaMessages('error','Error','กรอกข้อมูลไม่ครบ กรุณาลองใหม่.' );
            }
        } catch (error) {
            this.showErrorViaMessages('error','Error','บันทึกสำเร็จไม่สำเร็จ'  );

        }
    }

    // function update data
    async update() {
        let id = this.periodsIdEdit;
        let body = {
            period_name: this.periodsNameEdit,
            is_active: this.periodsActiveEdit,
            start_time: this.start_time,
            end_time: this.end_time
        };

        // update data
        const res:any = await this.periodsService.update(id, body);

        let message_upd = [];
            message_upd = res.data;
            if(message_upd.ok){
                this.showSuccessViaToast('success','Success','บันทึกสำเร็จ');
                this.messages = [];
            } else {
                this.showErrorViaMessages('error','Error','บันทึกสำเร็จไม่สำเร็จ'  );
            }
        // close form
        this.displayForm = false;

        // refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false
        };
        await this.periodsService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true
        };
        await this.periodsService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({behavior: 'smooth'});
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return {'background-image': 'url(' + path + image + ')'};
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }


    showErrorViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail  });
    }

    showSuccessViaToast(severity: any, summary: any, detail: any) {
        this.messageService.add({  severity: severity, summary:summary, detail: detail });
    }

    showErrorViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

    showSuccessViaMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

}
