import { Component } from '@angular/core';
import { LayoutService } from '../../../layout/service/app.layout.service';
import axios from 'axios';
import { ServicesService } from './services.service';
import { Message, MessageService } from 'primeng/api';
import { LookupService } from '../../services/lookup.service'
import { Table } from 'primeng/table';
// import { UploadEvent } from 'primeng/fileupload';

interface UploadEvents {
    originalEvent: Event;
    files: File[];
}

@Component({
    selector: 'app-backend-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss'],
    providers: [MessageService]

})


export class ServicesComponent {


    constructor(
        private layoutService: LayoutService,
        private servicesService: ServicesService,
        private lookupService: LookupService,
        private messageService: MessageService
    ) {
        //    console.log(sessionStorage.getItem("hospitalId")); 
        //   this.clinic =  this.clinics;

    }

    urlIcone: any;
    urlIconeEdit: any;

    columnRows: number = 1;
    hospitalID = sessionStorage.getItem("hospitalId");

    clinics: any;
    selectedClinic: any;
    services: any;
    displayForm: boolean = false;

    hospital_id: any;
    serviceName: string = '';
    serviceActive: boolean = true;

    serviceIdEdit: number = 0;
    serviceNameEdit: string = '';
    serviceActiveEdit: boolean = true;

    note: any;
    isAdd: boolean = false;
    isEdit: boolean = false;
    serviceNote: string = '';
    isSpiner: boolean = false;

    msgs: Message[] = [];

    messages: any | undefined;


    async ngOnInit() {

        await this.getData();
        await this.getClinics()
    }

    onBasicUploadAuto(event: UploadEvents) {
        this.messageService.add({ severity: 'info', summary: 'Success', detail: 'File Uploaded with Auto Mode' });
    }

    
    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },

            // { severity: 'success', summary: 'Success', detail: 'Message Content' },
            // { severity: 'info', summary: 'Info', detail: 'Message Content' },
            // { severity: 'warn', summary: 'Warning', detail: 'Message Content' },
            // { severity: 'error', summary: 'Error', detail: 'Message Content' }
        ];
    }
    // alert showToast
    showToast(severity: any, summary: any, detail: any) {
        this.messageService.add({ severity: severity, summary: summary, detail: detail });
    }
    clearMessages() {
        this.messages = [];
    }


    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    async getClinics() {
        const res: any = await this.lookupService.listClinic();
        let clinic = [];
        clinic = res.data;

        if (clinic.results.length > 0) {
            this.clinics = clinic.results;
            this.clinics = this.clinics.filter((s: any) => s.is_active);
        } else {
            alert('Error loading clinics');
        }

    }
    // get Services data from API
    async getData() {
        this.isSpiner = true;
        const res: any = await this.servicesService.list();
        let servicess = [];
        servicess = res.data;
        this.showSuccessViaMessages();

        if (servicess.results.length > 0) {
            this.isSpiner = false;
            this.services = servicess.results;
        } else {
            alert('Error loading clinics');
        }
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
    }

    // display form add data
    displayFormAdd() {
        this.isSpiner = true;
        this.messages='';
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;

        this.serviceName = '';
        this.hospital_id = this.hospitalID;
        this.serviceNote = '';
        this.urlIcone = '';
        this.isSpiner = false;
    }
    // display form edit data
    displayFormEdit(data: any) {
        this.messages='';
        this.isAdd = false;
        this.isEdit = true;
        this.selectedClinic = data.service_id;
        this.serviceIdEdit = data.service_type_id;
        this.serviceNameEdit = data.service_type_name;
        this.serviceActiveEdit = data.is_active;
        this.serviceNote = data.note;
        this.displayForm = true;
        this.urlIconeEdit = data.icon_filename;
    }

    // function save data
    async save() {

        try {

            if (this.serviceName != '' && this.serviceName != null && this.serviceName != undefined) {
                this.isSpiner = true;
                let data = {
                    service_type_name: this.serviceName,
                    service_id: this.selectedClinic,
                    is_active: this.isAdd,
                    hospital_id: this.hospital_id,
                    note: this.serviceNote,
                    icon_filename: "assets/images/"+this.urlIcone+".png"
                };
                
                // save data
                let res: any = await this.servicesService.save(data);

                if (res.status == 200) {
                    //refresh data
                    await this.getData();
                    this.isSpiner = false;
                    this.showToast('success', 'Success', 'บันทึกสำเร็จ')

                } else {
                    this.showMessages('error', 'Error', 'บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่.')

                }

                this.displayForm = false;
            } else {
                this.showMessages('error', 'Error', 'กรุณาระบุชื่อบริการ!')

            }
        } catch (error) {
            this.showMessages('error', 'Error', "error="+error)
            console.log(error);

        }

    }

    // function update data
    async update() {
        this.isSpiner = true;
        try {
            let id = this.serviceIdEdit;
            let body = {
                service_id: this.selectedClinic,
                service_type_name: this.serviceNameEdit,
                is_active: this.serviceActiveEdit,
                note: this.serviceNote,
                icon_filename: this.urlIconeEdit
            };

            if (this.serviceNameEdit != '') {

                // update data
                let res: any = await this.servicesService.update(id, body);
                if (res.status == 200) {
                    this.showToast('success', 'Success', 'บันทึกสำเร็จ')
                    // close form
                    this.displayForm = false;

                    // refresh data
                    await this.getData();
                    this.isSpiner = false;

                }else{
                    this.showMessages('error', 'Error', 'บันทึกสำเร็จไม่สำเร็จ')
                    this.isSpiner = false;
                }
               
            } else {
                this.showMessages('error', 'Error', 'กรอกข้อมูลไม่ครบ กรุณาลองใหม่.')
                this.isSpiner = false;
            }
        } catch (error) {
            this.showMessages('error', 'Error', 'บันทึกสำเร็จไม่สำเร็จ')
            this.isSpiner = false;
        }

    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false
        };
        await this.servicesService.update(id, body);
        this.showToast('success', 'Success', 'ปิดการใช้งาน')
        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true
        };
        this.showToast('success', 'Success', 'เปิดการใช้งาน')
        await this.servicesService.update(id, body);

        // refresh data
        await this.getData();
    }
    showSuccessViaMessages() {
        this.msgs = [];
        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Message sent' });
    }

}