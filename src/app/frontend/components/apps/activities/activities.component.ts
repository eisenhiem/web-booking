import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from './activities.service';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent {

  listServices: any = [];
  listClinics: any = [];
  selectedService: any = null;


  isLoading: boolean = false;

  constructor(
    private servicesService: ServicesService,
    private router: Router
  ) { }

  async ngOnInit() {
    await this.getServices();
  }

  async getServices() {
    this.isLoading = true;
    try {
      const response = await this.servicesService.list();
      let data = response.data;
      // console.log(data);

      if (data.ok) {
        this.listServices = data.results;
      }
      this.isLoading = false;
    } catch (error) {
      console.log(error);
    }
  }

  async calendar(id: number) {
    sessionStorage.setItem('serviceTypeId', id.toString());
    try {
      const res: any = await this.servicesService.listSlot(id);
      let data = res.data;
      if (data.ok) {
        sessionStorage.setItem('slots', JSON.stringify(data.results));
      }
    } catch (error) {
      console.log(error);
    }
    this.router.navigate(['/frontend/apps/hospitals']);
  }

}
