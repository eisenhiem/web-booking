import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { TimelineModule } from 'primeng/timeline';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { HospitalRoutingModule } from './hospital-routing.module';
import {HospitalsComponent} from './hospitals.component';
import { BlockUIModule } from 'primeng/blockui';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [HospitalsComponent],
  imports: [
    CommonModule,
    HospitalRoutingModule,
    CardModule,
    TimelineModule,
    AvatarGroupModule,
    AvatarModule,
    BlockUIModule,
    TableModule,
    TagModule,
    CheckboxModule,
    FormsModule,
    ButtonModule
  ]
})
export class HospitalModule { }
