import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class SlotService {
    // pathPrefix: any = `:40014`
    // pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

  
    async list(id:number) {
        const url = `/frontend/lookup/getLookupDateSlot/`+ id;
        return this.axiosInstance.get(url);
    }

    async saveCustomer(data:any) {
        const url = `/customers`;
        return this.axiosInstance.post(url,data);
    }

    async checkCustomer(data:any) {
        const url = `/customers/check/`+ data;;
        return this.axiosInstance.get(url);
    }

    async saveReserves(data:any) {
        const url = `/reserves`;
        return this.axiosInstance.post(url,data);
    }

}