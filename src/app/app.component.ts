import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { el } from '@fullcalendar/core/internal-common';
import { PrimeNGConfig } from 'primeng/api';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(private primengConfig: PrimeNGConfig,
        private router: Router,
        ) { }

    ngOnInit(): void {
        this.primengConfig.ripple = true;
        let token = sessionStorage.getItem('token');
        let roleId = sessionStorage.getItem('userRole');
        if(token){
            if(roleId == '2' || roleId =='3'){
                this.router.navigate(['/backend']);
            } else if(roleId =='1'){
                this.router.navigate(['/backend/user'])
            } else {
                this.router.navigate(['/frontend']);
            }
        }else{
            this.router.navigate(['auth/login']);
            
        }
    }
}